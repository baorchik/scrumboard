﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlobMetadataChanger
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = args[0];
            var containerName = args[1];
            var storageAccount = CloudStorageAccount.Parse(connectionString);
            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference(containerName);
            var blobs = container.ListBlobs(useFlatBlobListing: true).Cast<CloudBlockBlob>();

            foreach (var blob in blobs)
            {
                UpdateBlobContentTypeIfExtensionMatch(blob, ".html", "text/html; charset=UTF-8");
                UpdateBlobContentTypeIfExtensionMatch(blob, ".css", "text/css; charset=UTF-8");
                UpdateBlobContentTypeIfExtensionMatch(blob, ".js", "application/javascript");
                UpdateBlobContentTypeIfExtensionMatch(blob, ".png", "image/png");
                UpdateBlobContentTypeIfExtensionMatch(blob, ".gif", "image/gif");
                UpdateBlobContentTypeIfExtensionMatch(blob, ".jpg", "image/jpg");
            }                 
        }

        static void UpdateBlobContentTypeIfExtensionMatch(CloudBlockBlob blob, string extension, string contentType)
        {
            if (blob.Name.EndsWith(extension))
            {
                blob.Properties.ContentType = contentType;
                blob.SetProperties(); 
            }
        }
    }
}
