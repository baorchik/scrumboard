var express = require("express");
var azure = require("azure");
var _ = require("underscore");

function createTableService()
{
    return azure.createTableService(
        "your_storage_acount",
        "your_storage_account_key");
}

var stickersTableName = "Stickers";

exports.post = function(request, response)
{
    console.log(request.body);
    var validationResult = validateBoard(request.body);
     
    var tableService = createTableService();
    
    tableService.createTableIfNotExists(stickersTableName, function(error)
    {
        if (!error)
        {
            console.log("Table Stickers created or existed");
            saveBoard(tableService, request.body, response);
        }
        else
        {
            console.error(error);
            response.send(statusCodes.INTERNAL_SERVER_ERROR, { message: "Table creation failed. " + error });
        }
    });
};

function saveBoard(tableService, boardModel, response)
{
	tableService.beginBatch();
	_.each(
		boardModel.stickers,
		function(sticker)
		{
			sticker.PartitionKey = boardModel.boardId;
			sticker.RowKey = sticker.id;
			tableService.insertOrReplaceEntity(stickersTableName, sticker);
		});

	tableService.commitBatch(
		function(error, operationResponses, batchResponse)
		{
			var batchErrors = "";
			_.each(
				operationResponses,
				function(opresp)
				{
					if (opresp.error)
						batchErrors += opresp.error
				});

			console.log("Stickers saved. Error: " + error + " " + batchErrors);
			console.log(operationResponses);
			console.log(batchResponse);

			if (error || batchErrors !== "")
			{
				response.send(statusCodes.INTERNAL_SERVER_ERROR, { message : error + " " + batchErrors });
			}
			else
			{
				response.send(statusCodes.OK, { message : 'Batch commited!' });
			}
		});
}

exports.get = function(request, response)
{
    console.log(request);
    var boardId = request.query.boardId;
    if (!boardId)
    {
        response.send(statusCodes.BAD_REQUEST, { message : 'boardId is undefined.' });
    }
    
    var query = azure.TableQuery.select()
        .from(stickersTableName)
        .where('PartitionKey eq ?', boardId);
    
    var tableService = createTableService();
    tableService.queryEntities(
        query, 
        function(error, entities)
		{
            if(!error)
			{
                response.send(statusCodes.OK, entities);
            }
            else
            {
                response.send(statusCodes.INTERNAL_SERVER_ERROR, error);
            }
        });
};


function validateBoard(boardModel)
{
    
}