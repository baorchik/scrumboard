/*jshint browser:true, laxbreak:true, undef:true*/
var StorageClient = function()
{
	this.azureClient = new WindowsAzure.MobileServiceClient(
		'https://your_app.azure-mobile.net/',
		'your_application_key_goes_here');
};

StorageClient.prototype.saveBoard = function(data)
{
	this.azureClient
		.invokeApi(
			"board",
			{
				method: "POST",
				body: data
			})
		.done(
			function(response)
			{
				console.log(response);
				$.jGrowl("Board saved!");
			},
			function(error)
			{
				var xhr = error.request;
				$.jGrowl("Error calling azure API " + xhr.status + " " + xhr.responseText);
			});
};

StorageClient.prototype.loadBoard = function(boardId, onBoardLoaded, onBoardLoadFailed)
{
	var query = this.azureClient
		.invokeApi(
			"board",
			{
				method: "GET",
				parameters: { boardId: boardId }
			})
		.done(
			function(xmlHttpRequest)
			{
				console.log(xmlHttpRequest);
				onBoardLoaded(xmlHttpRequest);
			},
			function(error)
			{
				var xhr = error.request;
				onBoardLoadFailed(xhr);
			});
};